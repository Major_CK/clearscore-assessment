# README #

Clearscore Technical Assessment Solution.
Xola Sikeyi

### Solution Dependencies ###

* Dagger2 
* Retrofit
* rxJava
* Espresso
* Mockito
* LifecycleExt
* [donutview](https://github.com/futuredapp/donut) graph and animation

### Solution Layout ###
![alt text](https://bitbucket.org/Major_CK/clearscore-assessment/src/master/clearscore_technical_assessment.png)
