package com.xola.clearscore


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.xola.clearscore.util.EspressoIdlingResource
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun registerIdleResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unregisterIdleResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    @Test
    fun testShowCorrectViewOnLaunch() {
        onView(withId(R.id.main_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_get_score)).check(matches(isDisplayed()))
        onView(withId(R.id.img_logo)).check(matches(isDisplayed()))
        onView(withId(R.id.score_donut)).check(matches(isDisplayed()))

        onView(withId(R.id.donut_view)).check(matches(isDisplayed()))

        onView(withId(R.id.progressBar)).check(matches(not(isDisplayed())))
        onView(withId(R.id.tv_first_caption)).check(matches(not(isDisplayed())))
        onView(withId(R.id.tv_score)).check(matches(not(isDisplayed())))
        onView(withId(R.id.tv_third_caption)).check(matches(not(isDisplayed())))

    }

    @Test
    fun testShowResultsonButtonClick() {
        onView(withId(R.id.btn_get_score)).perform(click())

        onView(withId(R.id.img_logo)).check(matches(not(isDisplayed())))
        onView(withId(R.id.score_donut)).check(matches(isDisplayed()))
        onView(withId(R.id.donut_view)).check(matches(isDisplayed()))

        onView(withId(R.id.progressBar)).check(matches(not(isDisplayed())))
        onView(withId(R.id.tv_first_caption)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_score)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_third_caption)).check(matches(isDisplayed()))


    }
}
