package com.xola.clearscore

import android.animation.ValueAnimator
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import app.futured.donut.DonutSection
import com.google.android.material.snackbar.Snackbar
import com.xola.clearscore.models.CreditReportInfo
import com.xola.clearscore.util.EspressoIdlingResource
import com.xola.clearscore.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var viewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        observeViewModel()

        btn_get_score.setOnClickListener {
            EspressoIdlingResource.increament()
            viewModel.refresh()

        }
        initSetup()
    }

    private fun donutAnimation() {
        ValueAnimator.ofFloat(0f, 1f).apply {
            duration = 10000
            interpolator = FastOutSlowInInterpolator()
            addUpdateListener {
                score_donut.masterProgress = it.animatedValue as Float
                score_donut.alpha = it.animatedValue as Float
            }
            start()
        }
    }

    private fun updateInfo(info: CreditReportInfo) {
        val sections = listOf(
            DonutSection(
                getString(R.string.donut_section_category),
                Color.BLACK,
                info.score.toFloat()
            )

        )

        score_donut.submitData(sections)
        donutAnimation()
        img_logo.visibility=View.INVISIBLE
        tv_score.visibility = View.VISIBLE
        tv_first_caption.visibility =View.VISIBLE
        tv_third_caption.visibility = View.VISIBLE

        tv_first_caption.text = getString(R.string.head_caption)
        tv_score.text = info.score.toInt().toString() // Int to remove commas
        tv_third_caption.text = getString(R.string.max_score_comment).plus(info.maxScoreValue)

    }

    private fun initSetup() {
        score_donut.cap = 700f
        score_donut.masterProgress = 0.5f
        score_donut.gapAngleDegrees = -90f
    }

    private fun observeViewModel() {
        viewModel.root.observe(
            this,
            Observer { root ->
                root?.let {
                    EspressoIdlingResource.decrement()
                    updateInfo(it.creditReportInfo)

                }
            })
        viewModel.isLoading.observe(this, Observer { isLoading ->
            isLoading?.let {
                progressBar.visibility = if (it) View.VISIBLE else View.GONE


            }
        })

        viewModel.loadError.observe(this, Observer { loaderror ->
            loaderror?.let {

                if(it){
                    Snackbar.make(main_layout, getString(R.string.networke_exception_msg), Snackbar.LENGTH_LONG).show()
                }




            }
        })
    }
}