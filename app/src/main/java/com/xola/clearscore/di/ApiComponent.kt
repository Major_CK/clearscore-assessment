package com.xola.clearscore.di

import androidx.lifecycle.ViewModel
import com.xola.clearscore.requests.ApiService
import com.xola.clearscore.requests.ClearScoreAPI
import com.xola.clearscore.viewmodels.MainViewModel
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject(service: ApiService)
    fun inject(viewModel: MainViewModel)
}