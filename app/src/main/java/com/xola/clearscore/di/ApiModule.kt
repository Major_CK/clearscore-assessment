package com.xola.clearscore.di

import com.xola.clearscore.requests.ApiService
import com.xola.clearscore.requests.ClearScoreAPI
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApiModule {

    @Provides
    fun providesClearScoreApi(): ClearScoreAPI {
        return Retrofit.Builder()
            .baseUrl("https://android-interview.s3.eu-west-2.amazonaws.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ClearScoreAPI::class.java)

    }

    @Provides
    fun provideApiService(): ApiService {
        return ApiService()
    }
}