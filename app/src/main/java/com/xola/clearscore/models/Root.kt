package com.xola.clearscore.models

data class Root(
    val accountIDVStatus: String,
    val augmentedCreditScore: Any,
    val coachingSummary: CoachingSummary,
    val creditReportInfo: CreditReportInfo,
    val dashboardStatus: String,
    val personaType: String
)