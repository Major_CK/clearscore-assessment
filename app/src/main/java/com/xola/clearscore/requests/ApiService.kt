package com.xola.clearscore.requests

import com.xola.clearscore.di.DaggerApiComponent
import com.xola.clearscore.models.Root
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class ApiService {

    @Inject
    lateinit var  api: ClearScoreAPI

    init {
        DaggerApiComponent.create().inject(this)
    }

    fun getRoot(): Single<Root> {
        return  api.getRoot()
    }

}