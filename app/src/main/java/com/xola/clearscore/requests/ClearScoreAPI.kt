package com.xola.clearscore.requests

import com.xola.clearscore.models.Root
import io.reactivex.Single
import retrofit2.http.GET

interface ClearScoreAPI {

    @GET("endpoint.json")
    fun getRoot(): Single<Root>
}