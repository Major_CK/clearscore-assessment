package com.xola.clearscore.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.xola.clearscore.di.DaggerApiComponent
import com.xola.clearscore.models.Root
import com.xola.clearscore.requests.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel: ViewModel() {

    @Inject
    lateinit var apiService : ApiService
    init {
        DaggerApiComponent.create().inject(this)
    }

    private val disposable = CompositeDisposable()

    val root = MutableLiveData<Root>()
    val loadError = MutableLiveData<Boolean>()
    val isLoading = MutableLiveData<Boolean>()




    private fun getRootObject() {
        isLoading.value=true;
        disposable.add(
            apiService.getRoot()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Root>(){
                    override fun onSuccess(value: Root?) {
                        root.value=value
                        loadError.value=false
                        isLoading.value=false
                    }

                    override fun onError(e: Throwable?) {
                        loadError.value=true
                        isLoading.value=false
                    }
                }))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun refresh() {
        getRootObject()
    }
}