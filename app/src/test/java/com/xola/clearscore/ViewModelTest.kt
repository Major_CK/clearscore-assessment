package com.xola.clearscore

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.xola.clearscore.models.CoachingSummary
import com.xola.clearscore.models.CreditReportInfo
import com.xola.clearscore.models.Root
import com.xola.clearscore.requests.ApiService
import com.xola.clearscore.viewmodels.MainViewModel
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class ViewModelTest {
    @get: Rule
    var rule = InstantTaskExecutorRule()

    @Mock
    lateinit var apiService: ApiService

    @InjectMocks
    var viewModel = MainViewModel()

    private var testSingle: Single<Root>? = null

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getRootObjectOnSuccess() {
        val root = Root(
            "PASS", Any(),
            CoachingSummary(false, false, 1, 1, false),
            CreditReportInfo(
                1, 1, 1, ""
                , Any(), Any(), 1, 1, 1, 1,
                1, 1, 1, 1, "",
                false, false, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, ""
            ), "",
            ""
        )
        testSingle = Single.just(root)

        `when`(apiService.getRoot()).thenReturn(testSingle)

        viewModel.refresh()
        Assert.assertNotNull(viewModel.root.value)
        Assert.assertEquals(false, viewModel.isLoading.value)
        Assert.assertEquals(false, viewModel.loadError.value)

    }

    @Test
    fun getRootObjectOnFail() {
        val e = Throwable()
        testSingle = Single.error(e)

        `when`(apiService.getRoot()).thenReturn(testSingle)
        viewModel.refresh()

        Assert.assertNull(viewModel.root.value)
        Assert.assertEquals(false, viewModel.isLoading.value)
        Assert.assertEquals(true, viewModel.loadError.value)


    }

    @Before
    fun setUpRxSchedulers() {
        val immediate = object : Scheduler() {
            override fun scheduleDirect(run: Runnable?, delay: Long, unit: TimeUnit?): Disposable {
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }
        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }


    }

}